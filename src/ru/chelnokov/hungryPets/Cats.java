package ru.chelnokov.hungryPets;

public class Cats extends Pets{
    private String name;

    Cats(String name, int levelOfSatisfaction){
        super(name, levelOfSatisfaction);
    }

    @Override
    public String toString(){
        return "Имя: " + name + ", Уровень сытости: " + getLevelOfSatisfaction() ;
    }

    /**
     * имитирует голос кошки
     */
    public void voice(){
        System.out.println("Мяу-мяу-мяу!");
    }

    /*
     * метод еды для кошки(Повышает уровень сытости до максимума)
     */
    public void food(){
        int levelOfSatisfaction1 = getLevelOfSatisfaction();
        do{
            levelOfSatisfaction1++;
        }while(levelOfSatisfaction1<100);
        setLevelOfSatisfaction(levelOfSatisfaction1);
    }

    /*
     * метод игры для кошки (Понижает уровень сытости)
     */
    void play(){
        int satisfactionLevel = getLevelOfSatisfaction() - 15;
        setLevelOfSatisfaction(satisfactionLevel);
    }

    /*
     * метод сна для кошки(Понижает уровень сытости)
     */
    public void sleep(){
        int satisfactionLevel = getLevelOfSatisfaction() - 7;
        setLevelOfSatisfaction(satisfactionLevel);
    }
}

