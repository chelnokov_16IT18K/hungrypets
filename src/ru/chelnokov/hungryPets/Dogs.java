package ru.chelnokov.hungryPets;


public class Dogs extends Pets {
    private String name;
    Dogs(String name, int levelOfSatisfaction){
    super(name, levelOfSatisfaction);
}

    @Override
    public String toString(){
        return "Имя: " + name + ", Уровень сытости: " + getLevelOfSatisfaction() ;
    }

    /*
     * имитирует голос собаки
     */
    public void voice(){
        System.out.println("Гав-гав");
    }

    /*
     * кормит пса(Повышает уровень сытости до максимума)
     */
    public void food(){
        int levelOfSatisfaction = getLevelOfSatisfaction();
        do{
            levelOfSatisfaction++;
        }while(levelOfSatisfaction<100);
        setLevelOfSatisfaction(levelOfSatisfaction);
    }

    /*
     * метод для игры собаки
     */
    public void play(){
        int satisfactionLevel = getLevelOfSatisfaction() - 30;
        setLevelOfSatisfaction(satisfactionLevel);
    }

    /*
     * метод сна для собаки(Понижает уровень сытости)
     */
    public void sleep(){
        int satisfactionLevel = getLevelOfSatisfaction() - 10;
        setLevelOfSatisfaction(satisfactionLevel);
    }
}
