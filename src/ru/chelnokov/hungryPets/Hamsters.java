package ru.chelnokov.hungryPets;

public class Hamsters extends Pets {
    private String name;
    Hamsters(String name, int levelOfSatisfaction){
        super(name, levelOfSatisfaction);
    }

    @Override
    public String toString(){
        return "Имя: " + name + ", Уровень сытости: " + getLevelOfSatisfaction() ;
    }

    /**
     * имитирует голос хомяка
     */
    public void voice(){
        System.out.println("hshwrlbrlw...");
    }

    /*
     * метод еды для хомяка(Повышает уровень сытости до максимума)
     */
    public void food(){
        int levelOfSatisfaction = getLevelOfSatisfaction();
        do{
            levelOfSatisfaction++;
        }while(levelOfSatisfaction<100);
        setLevelOfSatisfaction(levelOfSatisfaction);
    }

    /*
     * метод игры для хомяка (Понижает уровень сытости)
     */
    void play(){
        int satisfactionLevel = getLevelOfSatisfaction() - 5;
        setLevelOfSatisfaction(satisfactionLevel);
    }

    /*
     * метод сна для хомяка(Понижает уровень сытости)
     */
    public void sleep(){
        int satisfactionLevel = getLevelOfSatisfaction() - 2;
        setLevelOfSatisfaction(satisfactionLevel);
    }
}
