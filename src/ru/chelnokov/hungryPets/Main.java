package ru.chelnokov.hungryPets;

public class Main {
    public static void main(String args[]){
        Cats cat = new Cats( "Cat", (int) (Math.random() * 71 + 30));
        Dogs dog = new Dogs("Dog",  (int) (Math.random() * 71 + 30));
        Hamsters hamster = new Hamsters("Hamster",  (int) (Math.random() * 71 + 30));
        cat.play();
        dog.sleep();
        hamster.play();
        hamster.sleep();
        cat.voice();
        dog.voice();
        hamster.voice();

        Pets[] pets = {cat, dog, hamster};
        printArrayElements(pets);

        Pets hungryPet = theMostHungry(pets);
        System.out.println("Самый голодный питомец:");
        System.out.println(hungryPet);
        hungryPet.food();
        System.out.println("Так и быть, покормим тебя");
        System.out.println(hungryPet);
    }

    /**
     * Возвращает питомца с наименьшим уровнем сытости
     *
     * @param pets массив питомцев
     * @return питомец с наименьшим уровнем сытости
     */
    private static Pets theMostHungry(Pets[] pets) {
        Pets hungryPet = null;
        int minimumSatisfaction = 100;
        for (Pets pet : pets){
            int satisfaction = pet.getLevelOfSatisfaction();
            if(minimumSatisfaction>satisfaction ){
                minimumSatisfaction = satisfaction;
                hungryPet = pet;
            }
        }
        return hungryPet;
    }

    /**
     * Выводит в консоль содержимое массива, состоящего из объектов
     *
     * @param objects массив объектов
     */
    private static void printArrayElements(Object[] objects) {
        for (Object object : objects) {
            System.out.println(object);
        }
    }


}
