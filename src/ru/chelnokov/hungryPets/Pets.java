package ru.chelnokov.hungryPets;

/**
 * Абстрактный класс с абстрактными методами
 */
public abstract class Pets {
    private String name;
    private int levelOfSatisfaction;
    Pets(String name, int levelOfSatisfaction){
        this.name = name;
        this.levelOfSatisfaction = levelOfSatisfaction;
    }

    String getName(){
        return name;
    }
    int getLevelOfSatisfaction(){
        return  levelOfSatisfaction;
    }
    public abstract void food();//абстрактные методы
    public abstract void sleep();
    public abstract void voice();

    void setLevelOfSatisfaction(int levelOfSatisfaction) {
        this.levelOfSatisfaction = levelOfSatisfaction;
    }
}
